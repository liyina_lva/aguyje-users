var express = require('express');
var mongoose = require('mongoose');
var { createLogger, format, transports } = require('winston');
var { combine, timestamp, label, printf } = format;
var bodyParser = require('body-parser');
var jackrabbit = require('jackrabbit');
var rabbitUrl = "amqp://pconpuub:Y8IjBDtuqg2BfSnGIkRXAb0bYjEgMbax@lion.rmq.cloudamqp.com/pconpuub";
var rabbit = jackrabbit(rabbitUrl);
var exchange = rabbit.default(); 

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));  

const myFormat = printf(info => {
    return `${info.timestamp} ${info.level}: ${info.message}`;
  });

  const logger = createLogger({
    format: combine(
      timestamp(),
      myFormat
    ),
    transports: [
        new transports.Console(),
        new transports.File({ filename: 'aguyje.log' })]
  });

mongoose.connect('mongodb+srv://gabs:123@cluster0-voanl.gcp.mongodb.net/test?retryWrites=true');

var UserSchema = new mongoose.Schema({
    name: {type: String},
    username: {type: String, unique: true },
    qtyOfKudos: {type: Number, default: 0}
});
UserSchema.index({ username: 1}, { unique: true});
var User = mongoose.model('User', UserSchema);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    logger.info("Connected with MongoDB Atlas");
});

var updateKu2Counter = exchange.queue({ name: 'updateKu2Counter', durable: false });
updateKu2Counter.consume(onupdateKu2Counter);

function onupdateKu2Counter(data, ack) {
    logger.info("newKudos Received for user: " + data.msg);
    ack();
    updateKudosQuantityByUserId(data.msg);
}

function updateKudosQuantityByUserId(user_id){
    User.findById({_id: user_id}, function(err,user){
        if (err){
            return err;
        }
        if(user.qtyOfKudos){
            var qtyOfKudos = user.qtyOfKudos + 1;
        }else{
            var qtyOfKudos = 1;
        }
        user.update({'qtyOfKudos': qtyOfKudos},  function(err, user){
            if (err){
                return err;
            }
            logger.info("Updated qty");
        })
    })
}

var subtractKu2Counter = exchange.queue({ name: 'subtractKu2Counter', durable: false });
subtractKu2Counter.consume(onsubtractKu2Counter);

function onsubtractKu2Counter(data, ack) {
    logger.info("deletedku2 Received for user: " + data.msg);
    ack();
    subtractKudosQuantityByUserId(data.msg);
}

function subtractKudosQuantityByUserId(user_id){
    User.findById({_id: user_id}, function(err,user){
        if (err){
            return err;
        }
        if(user.qtyOfKudos){
            var qtyOfKudos = user.qtyOfKudos - 1;
        }else{
            var qtyOfKudos = 0;
        }
        user.update({'qtyOfKudos': qtyOfKudos},  function(err, user){
            if (err){
                return err;
            }
            logger.info("Subtract qty");
        })
    })
}

app.post('/api/users', function(req, res){
    var newUser = new User({
        name: req.body.name,
        username:  req.body.username,
        qtyOfKudos: req.body.qtyOfKudos
    });
    newUser.save(function (err, user) {
        if (err) return console.error(err);
        logger.info("[Creacion de Usuario] Usuario " + user.id);
      });
    res.send("Nuevo usuario " + newUser.name);
});

app.delete('/api/users/:userId', function(req, res) {
    User.findById({_id:req.params.userId}, function(err, user){
        if(!err){
            if (user){
                user.remove(function(error, response){
                    exchange.publish({ msg: response.id}, { key: 'deletedUser' });
                    logger.info("[Eliminacion de usuario] Usuario "+ response.id);
                    return res.send("Usuario eliminado " + response.id);
                });    
            }else{
                logger.error("[Eliminacion de usuario] No se pudo eliminar el usuario "+ req.params.userId);
            }
            
        }else{
            res.send("No se pudo eliminar el usuario "+ req.params.userId);
        }
    });    
    
});

app.get('/api/users', function(req, res){
    User.find(function(err,users){
        if (!err){
            logger.info("[Lista de Usuarios]");
            return res.send(users);
        }
        logger.error("[Lista de Usuarios] Error obteniendo la lista de usuarios");
        return res.send("Error obteniendo la lista de usuarios");
    })
});

app.get('/api/users/:receiverId/:senderId', function(req, res){
    User.findById({_id:req.params.receiverId},function(err,receiver){
        if (err){
            return err;
        }
        logger.info("[Lista de Usuarios]");
        if (!receiver){
            res.send({'Result':False});
        }else{
            User.findById({_id:req.params.senderId}, function(err, sender){
                if(err){
                    res.send(err);
                }
                logger.info("[Detalle Usuario] Usuario "+ req.params.userId)
                if (!sender){
                    res.send({'Result':'False'})
                }else{
                    res.send({'Result':'True'});
                }
            });
        }
    })
});

app.get('/api/users/:userId', function(req, res){
    User.findById({_id:req.params.userId}, function(err, user){
        if(!err){
            if(user){
                logger.info("[Detalle Usuario] Usuario " + req.params.userId)
                return res.send(user);
            }else{
                logger.error("[Detalle Usuario] Error obteniendo la informacion del usuario " + req.params.userId);
                return res.send("Error obteniendo la informacion del usuario " + req.params.userId);
            }
        }
        logger.error("[Detalle Usuario] Error obteniendo la informacion del usuario " + req.params.userId);
        return res.send("Error obteniendo la informacion del usuario " + req.params.userId);
    });
});

app.listen(3000);